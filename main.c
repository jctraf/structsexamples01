#include <stdio.h>

// 1. make the definition for your struct
struct date {
    int month;
    int day;
    int year;
};

// function definition
void printDate(struct date d);

int main() {
    // 2. Create an "instance" of the struct
    struct date birthday;

    // 3. set the properties of the struct
    birthday.day = 7;
    birthday.month = 12;
    birthday.year = 1998;

    // 4. print to the screen (get the properties)
    // yyyy-mm-dd
    printDate(birthday);

    struct date lastDayOfSemester;
    lastDayOfSemester.day = 15;
    lastDayOfSemester.month = 4;
    lastDayOfSemester.year = 2020;
    printDate(lastDayOfSemester);

    return 0;
}

// Exercise: Make a function that prints out the date
// Function should accept 1 parameter, of "struct date"
// Once it has the date, print it out in yyyy-mm-dd format
void printDate(struct date d) {
    printf("Your date is: %d-%02d-%02d\n",
           d.year,
           d.month,
           d.day);
}


